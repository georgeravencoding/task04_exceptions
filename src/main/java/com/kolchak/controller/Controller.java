package com.kolchak.controller;

public interface Controller {
    void declareUserNameInPassport();

    void declareUserSurnameInPassport();

    void declareUserAgeInPassport();

    void declareUserMarriageStatusInPassport();

    void savePassportData();

    void showPassportData();
}
