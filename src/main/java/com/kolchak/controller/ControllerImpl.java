package com.kolchak.controller;

import com.kolchak.model.Passport;
import com.kolchak.model.Person;


import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.util.Scanner;

public class ControllerImpl implements Controller {
    private Person person = new Person();
    private Passport passport = new Passport();
    private Scanner scanner = new Scanner(System.in);

    public void declareUserNameInPassport() {
        System.out.print("Input your name: ");
        String inputName = scanner.next();
        person.setName(inputName);
        passport.setPersonName(person.getName());
    }

    public void declareUserSurnameInPassport() {
        System.out.print("Input your surname: ");
        String inputSurname = scanner.next();
        person.setSurename(inputSurname);
        passport.setPersonSurname(person.getSurename());
    }

    public void declareUserAgeInPassport() {
        System.out.print("Input your age: ");
        int inputAge = scanner.nextInt();
        person.setAge(inputAge);
        passport.setPersonAge(person.getAge());
    }

    public void declareUserMarriageStatusInPassport() {
        System.out.print("Are you married? (y/n) ");
        String inputMarriageStatus = scanner.next();
        if (inputMarriageStatus.equalsIgnoreCase("y")) {
            person.setMarried(true);
        } else if (inputMarriageStatus.equalsIgnoreCase("n")) {
            person.setMarried(false);
        } else {
            System.out.println("Input \"y\" or \"n\"");
            declareUserMarriageStatusInPassport();
        }
        passport.setPersonIsMarried(person.isMarried());
    }

    public void savePassportData() {
        try {
            passport.setFile(new PrintWriter(person.getName() + "." + person.getSurename() + ".txt"));
            passport.setDateCreate(LocalDateTime.now());
            passport.getFile().println(passport.getDateCreate());
            passport.getFile().println("User name: " + person.getName());
            passport.getFile().println("User surname: " + person.getSurename());
            passport.getFile().println("Marriage status: " + person.isMarried());
            passport.getFile().println("User age: " + person.getAge());
            passport.getFile().close();

        } catch (FileNotFoundException e) {
            System.out.println("File not found!");
        }
//        if (passport.getFile() != null) {
//            passport.getFile().close();
//        }
    }

    public void showPassportData() {
        try {
            FileInputStream showPassport = new FileInputStream("/Users/qwerty/OneDrive - ELEKS/Personal/Java Studing/EPAM/Topic 5. Nested classes + Exception/homework/" + person.getName() + "." + person.getSurename() + ".txt");
            int i;
            while ((i = showPassport.read()) != -1) {
                System.out.print((char) i);
            }
        } catch (IOException e) {
            System.out.println("!!!Wrong path to file!!!");
        }
    }
}
