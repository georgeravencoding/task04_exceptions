package com.kolchak.model;

import java.io.PrintWriter;
import java.time.LocalDateTime;

public class Passport {
    private String personName;
    private String personSurname;
    private int personAge;
    private boolean personIsMarried;
    private LocalDateTime dateCreate;
    private PrintWriter file;

    public Passport() {
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getPersonSurname() {
        return personSurname;
    }

    public void setPersonSurname(String personSurname) {
        this.personSurname = personSurname;
    }

    public int getPersonAge() {
        return personAge;
    }

    public void setPersonAge(int personAge) {
        this.personAge = personAge;
    }

    public boolean isPersonIsMarried() {
        return personIsMarried;
    }

    public LocalDateTime getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(LocalDateTime dateCreate) {
        this.dateCreate = dateCreate;
    }

    public void setPersonIsMarried(boolean personIsMarried) {
        this.personIsMarried = personIsMarried;
    }

    public PrintWriter getFile() {
        return file;
    }

    public void setFile(PrintWriter file) {
        this.file = file;
    }

    @Override
    public String toString() {
        return "Passport data:" +
                "\nName: " + personName +
                "\nSurname: " + personSurname +
                "\nAge: " + personAge +
                "\nMarriage status: " + personIsMarried +
                '}';
    }
}
