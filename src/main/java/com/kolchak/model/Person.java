package com.kolchak.model;

public class Person {
    private String name;
    private String surname;
    private int age;
    private boolean isMarried;

    public Person() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurename() {
        return surname;
    }

    public void setSurename(String surename) {
        this.surname = surename;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isMarried() {
        return isMarried;
    }

    public void setMarried(boolean married) {
        isMarried = married;
    }


    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", surename='" + surname + '\'' +
                ", age=" + age +
                ", isMarried=" + isMarried +
                '}';
    }
}
