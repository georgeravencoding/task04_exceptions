package com.kolchak;

import com.kolchak.model.Passport;
import com.kolchak.view.MyView;
import com.kolchak.view.Printable;

public class Application {
    public static void main(String[] args) {
        new MyView().show();
    }
}
