package com.kolchak.view;

import com.kolchak.controller.Controller;
import com.kolchak.controller.ControllerImpl;
import com.kolchak.model.Passport;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public MyView() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - declare your name");
        menu.put("2", "  2 - declare your surname");
        menu.put("3", "  3 - declare your age");
        menu.put("4", "  4 - declare your marriage status");
        menu.put("5", "  5 - save the passport result ");
        menu.put("6", "  6 - show the passport result (hard cast path file, giving exception)");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
        methodsMenu.put("6", this::pressButton6);
    }

    private void pressButton1() {
        controller.declareUserNameInPassport();
    }

    private void pressButton2() {
        controller.declareUserSurnameInPassport();
    }

    private void pressButton3() {
        controller.declareUserAgeInPassport();
    }

    private void pressButton4() {
        controller.declareUserMarriageStatusInPassport();

    }

    private void pressButton5() {
        controller.savePassportData();
    }

    private void pressButton6() {
        controller.showPassportData();
    }

    private void inputMenu() {
        System.out.println("MENU");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        Passport passport = new Passport();
        do {
            inputMenu();
            System.out.println("Please select menu point: ");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {

            }
        } while (!keyMenu.equalsIgnoreCase("Q"));
    }
}
